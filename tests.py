from recall import (
    Results,
    evaluate_RtF,
)


def test_evaluate_RtF():
    W, O, L = Results.WIN, Results.WIN_ONLY_WITH_RECALL, Results.LOSS

    func = lambda results: list(evaluate_RtF(results))
    assert func([W, W, W]) == [W, W, W]
    assert func([O, O, O]) == [W, L, L]
    assert func([O, W, L]) == [W, W, L]
    assert func([O, O, L]) == [W, L, L]
