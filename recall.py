"""
Attempt to model the useful effects of recall the future.

Need
    - token bag model
    - ensure Recall can trigger only once a turn
    - model many over/under scenarios
"""
from collections import Counter
from enum import Enum, auto
from functools import partial
from itertools import chain
import random

from boltons.iterutils import chunked


class Tokens(Enum):
    # numeric
    ONE = 1
    ZERO = 0
    N_1 = -1
    N_2 = -2
    N_3 = -3
    N_4 = -4
    N_5 = -5
    N_6 = -6
    N_7 = -7
    N_8 = -8
    # special
    TENTACLE = 4
    SKULL = 5
    HOOD = 6
    TABLET = 7
    SQUID = 8
    STAR = 9


class Results(Enum):
    WIN = auto()
    LOSS = auto()
    WIN_ONLY_WITH_RECALL = auto()


class TokenBag:
    "The bag of tokens. Add convenience factories."
    def __init__(self, tokens):
        self._tokens = tuple(tokens)

    def draw(self):
        "Return a random token."
        return random.choice(self._tokens)

    def tokens(self):
        "Return a tuple of all tokens in the bag."
        return self._tokens

    @classmethod
    def current_bag(cls):
        return cls([
            Tokens.ONE,
            Tokens.ZERO,
            Tokens.ZERO,
            Tokens.N_1,
            Tokens.N_2,
            Tokens.N_3,
            Tokens.N_3,
            Tokens.N_4,
            Tokens.N_6,
            Tokens.SKULL,
            Tokens.SKULL,
            Tokens.HOOD,
            Tokens.SQUID,
            Tokens.TENTACLE,
            Tokens.STAR,
        ])


def simple_value_token(token):
    "Returns a value for the token."
    if token is Tokens.TENTACLE:
        return -99  # remember - simple
    elif token is Tokens.SKULL:
        return -4  # so that there are 2x -4 tokens
    elif token is Tokens.HOOD:
        return -3
    elif token is Tokens.SQUID:
        return -3
    elif token is Tokens.TABLET:
        return -4
    elif token is Tokens.STAR:
        return +1
    else:
        return token.value


# must provide token and not bag so that you can perfectly model win/fail
def calculate_pull_result(difficulty, base, value_token, token, modifier_func):
    """
    Return (check, succeeded)

    check: the final check value of the player
    succeeded: True if the player succeeded the check, False otherwise.
    """
    check_val = base + value_token(token) + modifier_func(token)
    check_val = max(check_val, 0)
    return check_val, check_val >= difficulty and token != Tokens.TENTACLE


def pick_RtF_token(bag, threshold, value_token):
    """
    Returns the optimal token for Recall the Future.

    Args:
        bag: the bag to pull from.
        threshold: the minimum value the token must negate. E.g., 4 indicates
                   you are 4 over the skill check, so only -5 and -6 tokens
                   can be usefully targetted.
        value_token: function to compute the value of a token.
    """
    to_consider = [token for token in bag.tokens()
                   if threshold > value_token(token) >= threshold - 2]
    by_count = list(Counter(to_consider).items())
    by_count.sort(key=lambda token_cnt: token_cnt[1], reverse=True)
    # TODO: always prefer non-numeric token
    return by_count[0][0]


def model_once(bag, difficulty, base, value_token):
    """
    Runs model once and returns result.

    Args:
        bag: the bag of tokens to draw from.
        difficulty: difficulty of skill check.
        base: base value of skill check by Investigator.
        value_token: function to convert a token to a negative/positive value.

    Returns one of WIN, LOSS, or WIN_ONLY_WITH_RECALL.
    """
    token = bag.draw()
    get_result = partial(calculate_pull_result,
                         difficulty, base, value_token, token)
    _, without_rcf = get_result(lambda token: 0)

    rtf_token = pick_RtF_token(bag, difficulty - base, value_token)
    _, with_rcf = get_result(lambda token: 2 if token == rtf_token else 0)
    if with_rcf and without_rcf:
        return Results.WIN
    elif with_rcf and not without_rcf:
        return Results.WIN_ONLY_WITH_RECALL
    else:
        return Results.LOSS


def model(over_under, *, samples=1_000_000):
    bag = TokenBag.current_bag()
    difficulty = 1
    base = difficulty + over_under
    pulls = [model_once(bag, difficulty, base, simple_value_token)
             for i in range(samples)]

    for num_pulls_per_turn in range(1, 5):
        pull_groups = chunked(pulls, num_pulls_per_turn)
        pull_results = chain.from_iterable(map(evaluate_RtF, pull_groups))
        no_rtf = chain.from_iterable(map(RtF_is_loss, pull_groups))
        summary = Counter(pull_results)
        summary_no_RtF = Counter(no_rtf)
        ratios = calculate_ratios(num_pulls_per_turn, summary, summary_no_RtF)  # noqa
        decay = (ratios[0] - ratios[1]) * 100  # as a percent
        print(f"Decay with {num_pulls_per_turn} pulls: {decay:.2f}%")


def evaluate_RtF(actions):
    """
    Evaluate Recall the Future actions in a sequence.

    Transforms the first Results.WIN_ONLY_WITH_RECALL: first value to
    Results.WIN. Subsequent values to Results.LOSS.
    """
    knelt = False
    for result in actions:
        if result is Results.WIN_ONLY_WITH_RECALL:
            if not knelt:
                knelt = True
                result = Results.WIN
            else:
                result = Results.LOSS
        yield result


def RtF_is_loss(actions):
    return [Results.LOSS if token is Results.WIN_ONLY_WITH_RECALL else token
            for token in actions]


def calculate_ratios(num_pulls_per_turn, with_RtF, without_RtF):
    "Returns (ratio_with_RtF, ratio_without_RtF)."
    def ratio(summary):
        win, loss = summary[Results.WIN], summary[Results.LOSS]
        ratio = win / (win + loss)
        return ratio

    return ratio(with_RtF), ratio(without_RtF)


if __name__ == '__main__':
    print("Decay Calculation - 2 targettable tokens")
    model(3)
    # Comments for 1M runs
    """
    Decay Calculation - 2 targettable tokens
    Decay with 1 pulls: 13.36%
    Decay with 2 pulls: 12.47%
    Decay with 3 pulls: 11.65%
    Decay with 4 pulls: 10.91%
    """
    print("Decay Calculation - 1 targettable tokens")
    model(4)
    """
    Decay Calculation - 1 targettable tokens
    Decay with 1 pulls: 6.62%
    Decay with 2 pulls: 6.40%
    Decay with 3 pulls: 6.19%
    Decay with 4 pulls: 5.99%
    """
